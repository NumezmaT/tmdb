import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:intl/intl.dart';

part 'movie_details.g.dart';

@JsonSerializable(anyMap: true)
class MovieDetails {
  MovieDetails(
      {@required this.id,
      @required this.title,
      @required this.overview,
      @required this.posterPath,
      @required this.backdropPath,
      @required this.popularity,
      @required this.releaseDate,
      @required this.voteAverage,
      @required this.genres});

  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "title")
  final String title;
  @JsonKey(name: "overview")
  final String overview;
  @JsonKey(name: "poster_path")
  final String posterPath;
  @JsonKey(name: "backdrop_path")
  final String backdropPath;
  @JsonKey(name: "popularity")
  final double popularity;
  @JsonKey(name: "release_date")
  final String releaseDate;
  @JsonKey(name: "vote_average")
  final double voteAverage;
  @JsonKey(name: "genres")
  final List<Genres> genres;

  String date;
  getReleaseDate() {
    if (date != null) {
      return date;
    }
    var dateTime = DateTime.parse(releaseDate);
    date = DateFormat("dd-MM-yyyy").format(dateTime);
    return date;
  }

  factory MovieDetails.fromJson(Map<String, dynamic> json) => _$MovieDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$MovieDetailsToJson(this);
}

@JsonSerializable(anyMap: true)
class Genres {
  Genres({@required this.id, @required this.name});

  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "name")
  final String name;

  factory Genres.fromJson(Map<String, dynamic> json) => _$GenresFromJson(json);

  Map<String, dynamic> toJson() => _$GenresToJson(this);
}
