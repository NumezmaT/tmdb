// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieDetails _$MovieDetailsFromJson(Map json) {
  return MovieDetails(
      id: json['id'] as int,
      title: json['title'] as String,
      overview: json['overview'] as String,
      posterPath: json['poster_path'] as String,
      backdropPath: json['backdrop_path'] as String,
      popularity: (json['popularity'] as num)?.toDouble(),
      releaseDate: json['release_date'] as String,
      voteAverage: (json['vote_average'] as num)?.toDouble(),
      genres: (json['genres'] as List)
          ?.map((e) => e == null
              ? null
              : Genres.fromJson((e as Map)?.map(
                  (k, e) => MapEntry(k as String, e),
                )))
          ?.toList());
}

Map<String, dynamic> _$MovieDetailsToJson(MovieDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'overview': instance.overview,
      'poster_path': instance.posterPath,
      'backdrop_path': instance.backdropPath,
      'popularity': instance.popularity,
      'release_date': instance.releaseDate,
      'vote_average': instance.voteAverage,
      'genres': instance.genres
    };

Genres _$GenresFromJson(Map json) {
  return Genres(id: json['id'] as int, name: json['name'] as String);
}

Map<String, dynamic> _$GenresToJson(Genres instance) =>
    <String, dynamic>{'id': instance.id, 'name': instance.name};
