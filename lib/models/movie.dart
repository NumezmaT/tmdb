import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'movie.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class ResponseData {
  ResponseData({@required this.page, @required this.totalResults, @required this.totalPages, @required this.results});

  @JsonKey(name: "page")
  final int page;
  @JsonKey(name: "total_results")
  final int totalResults;
  @JsonKey(name: "total_pages")
  final int totalPages;
  @JsonKey(name: "results")
  final List<Movie> results;

  factory ResponseData.fromJson(Map<String, dynamic> json) => _$ResponseDataFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseDataToJson(this);
}

@JsonSerializable(anyMap: true)
class Movie {
  Movie({@required this.id, @required this.title, @required this.posterPath, @required this.popularity});

  @JsonKey(name: "id")
  final int id;
  @JsonKey(name: "title")
  final String title;
  @JsonKey(name: "poster_path")
  final String posterPath;
  @JsonKey(name: "popularity")
  final double popularity;

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);

  Map<String, dynamic> toJson() => _$MovieToJson(this);
}
