import 'package:flutter/material.dart';
import 'package:movies_app/bloc/movie_bloc.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/ui/movie_details.dart';

class MovieListScreen extends StatefulWidget {
  MovieListScreen(this._type);

  final MovieType _type;

  @override
  State<StatefulWidget> createState() => _MovieListScreenState(_type);
}

class _MovieListScreenState extends State<MovieListScreen> {
  final MovieType _type;
  var _bloc = MovieBloc();

  _MovieListScreenState(this._type);

  @override
  void initState() {
    _bloc.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _bloc.getMovieStream(_type),
      builder: (context, AsyncSnapshot<List<Movie>> snapshot) {
        if (snapshot.hasData) {
          return buildList(snapshot);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget buildList(AsyncSnapshot<List<Movie>> snapshot) {
    return GridView.builder(
      padding: EdgeInsets.all(4),
      itemCount: snapshot.data.length,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 6 / 9,
        crossAxisSpacing: 4,
        mainAxisSpacing: 4,
      ),
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                fullscreenDialog: true,
                builder: (BuildContext context) => MovieDetailsScreen(snapshot.data[index]),
              ),
            );
          },
          child: Hero(
            tag: snapshot.data[index].id,
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FadeInImage.assetNetwork(
                  placeholder: "images/placeholder.png",
                  image: 'https://image.tmdb.org/t/p/w400${snapshot.data[index].posterPath}',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}

enum MovieType { topRate, upComing }
