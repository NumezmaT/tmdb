import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movies_app/bloc/movie_bloc.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movie_details.dart';

class MovieDetailsScreen extends StatefulWidget {
  MovieDetailsScreen(this._movie);

  final Movie _movie;

  @override
  State<StatefulWidget> createState() => _MovieDetailsScreenState(_movie);
}

class _MovieDetailsScreenState extends State<MovieDetailsScreen> {
  final Movie _movie;
  var _bloc = MovieBloc();

  _MovieDetailsScreenState(this._movie);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(title: Text(_movie.title)),
        body: StreamBuilder(
          stream: _bloc.getMovie(_movie.id),
          builder: (context, AsyncSnapshot<MovieDetails> snapshot) {
            if (snapshot.hasData) {
              return Container(child: buildDetails(snapshot));
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return _buildPoster(_movie.id, _movie.posterPath);
          },
        ),
      ),
    );
  }

  List<Widget> _buildCategoryChips(MovieDetails details) {
    return details.genres.map((genre) {
      return Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: Chip(
          label: Text(
            genre.name,
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
      );
    }).toList();
  }

  Widget _buildPoster(int id, String posterPath) {
    return Container(
      margin: new EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
      height: 180,
      child: Hero(
        tag: id,
        child: AspectRatio(
          aspectRatio: 6 / 9,
          child: FadeInImage.assetNetwork(
            placeholder: "images/placeholder.png",
            image: 'https://image.tmdb.org/t/p/w400$posterPath',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _buildRatingBar(MovieDetails _movie) {
    var stars = <Widget>[];
    var fillStars = _movie.voteAverage / 2;

    for (var i = 1; i <= 5; i++) {
      var color = i <= fillStars ? Colors.yellow[400] : Colors.white54;
      var star = Icon(
        Icons.star,
        color: color,
      );

      stars.add(star);
    }

    return Row(children: stars);
  }

  Widget buildInformation(MovieDetails _movie) {
    return Padding(
      padding: EdgeInsets.only(top: 8, right: 8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 12.0),
          Wrap(
            spacing: 8.0,
            runSpacing: 4.0,
            direction: Axis.horizontal,
            children: _buildCategoryChips(_movie),
          ),
          SizedBox(height: 8.0),
          Text(
            "Release date :${_movie.getReleaseDate()}",
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 16.0),
          ),
          SizedBox(height: 8.0),
          _buildRatingBar(_movie),
          SizedBox(height: 8.0),
          Text(
            'Overview',
            style: TextStyle(fontSize: 18.0),
          ),
          SizedBox(height: 8.0),
          Text(
            _movie.overview,
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 16.0),
          ),
        ],
      ),
    );
  }

  Widget buildDetails(AsyncSnapshot<MovieDetails> snapshot) {
    var _movie = snapshot.data;
    return Stack(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _buildPoster(_movie.id, _movie.posterPath),
            SizedBox(width: 16.0),
            Expanded(child: buildInformation(_movie)),
          ],
        ),
      ],
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
