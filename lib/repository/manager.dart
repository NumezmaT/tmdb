import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movie_details.dart';
import 'package:movies_app/repository/repository.dart';
import 'package:movies_app/repository/repository_memory.dart';

class RepoManager {
  static final RepoManager _singleton = RepoManager._internal();

  factory RepoManager() {
    return _singleton;
  }

  RepoManager._internal();

  var _repository = Repository.create();
  var _repositoryMemory = RepositoryMemory();

  Future<List<Movie>> getTopRate() async {
    var list = _repositoryMemory.getTopRate();

    if (list.isNotEmpty) {
      return list;
    }

    return _repository.getTopRate().then(
      (it) {
        var results = it.results;
        _repositoryMemory.putTopRate(results);
        return results;
      },
    ).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;
          Logger().e("Got error : ${res.statusCode} -> ${res.statusMessage}");
          break;
        default:
      }
    });
  }

  Future<List<Movie>> getUpComing() async {
    var list = _repositoryMemory.getUpComing();

    if (list.isNotEmpty) {
      return list;
    }

    return _repository.getUpComing().then(
      (it) {
        var results = it.results;
        _repositoryMemory.putUpComing(results);
        return results;
      },
    ).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;
          Logger().e("Got error : ${res.statusCode} -> ${res.statusMessage}");
          break;
        default:
      }
    });
  }

  Future<MovieDetails> getById(int id) async {
    var movie = _repositoryMemory.getById(id);

    if (movie != null) {
      return movie;
    }

    return _repository.getById(id).then(
      (it) {
        _repositoryMemory.putMovie(it);
        return it;
      },
    ).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;
          Logger().e("Got error : ${res.statusCode} -> ${res.statusMessage}");
          break;
        default:
      }
    });
  }
}
