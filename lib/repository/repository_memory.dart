import 'dart:collection';

import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movie_details.dart';

class RepositoryMemory {
  static final RepositoryMemory _singleton = RepositoryMemory._internal();

  List<Movie> _movieListTopRate = List();
  List<Movie> _movieListUpComing = List();
  HashMap<int, MovieDetails> _movieDetails = HashMap();

  factory RepositoryMemory() {
    return _singleton;
  }

  RepositoryMemory._internal();

  List<Movie> getTopRate() {
    return _movieListTopRate;
  }

  List<Movie> getUpComing() {
    return _movieListUpComing;
  }

  MovieDetails getById(int id) {
    return _movieDetails[id];
  }

  putTopRate(List<Movie> list) {
    _movieListTopRate.clear();
    _movieListTopRate.addAll(list);
  }

  putUpComing(List<Movie> list) {
    _movieListUpComing.clear();
    _movieListUpComing.addAll(list);
  }

  putMovie(MovieDetails movieDetails) {
    _movieDetails[movieDetails.id] = movieDetails;
  }
}
