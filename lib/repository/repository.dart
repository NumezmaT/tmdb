import 'package:movies_app/models/movie.dart';
import 'package:movies_app/models/movie_details.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'repository.g.dart';

@RestApi(baseUrl: "https://api.themoviedb.org/3")
abstract class Repository {
  static final _apiKey = 'cc7357511eaa7a56435768a6ce97dc88';
  static Repository _repository;

  static Repository create() {
    if (_repository != null) {
      return _repository;
    }

    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor());
    _repository = Repository(dio);
    return Repository(dio);
  }

  factory Repository(Dio dio) = _Repository;

  @GET("/movie/top_rated?api_key=cc7357511eaa7a56435768a6ce97dc88")
  Future<ResponseData> getTopRate();

  @GET("/movie/upcoming?api_key=cc7357511eaa7a56435768a6ce97dc88")
  Future<ResponseData> getUpComing();

  @GET("/movie/{id}?api_key=cc7357511eaa7a56435768a6ce97dc88")
  Future<MovieDetails> getById(@Path() int id);
}
