import 'dart:async';
import 'package:movies_app/models/movie.dart';
import 'package:movies_app/repository/manager.dart';
import 'package:movies_app/ui/movie_list.dart';

class MovieBloc {
  final _repository = RepoManager();
  final _moviesTopRate = StreamController<List<Movie>>();
  final _moviesUpComing = StreamController<List<Movie>>();

  Stream<List<Movie>> get moviesTopRate => _moviesTopRate.stream;

  Stream<List<Movie>> get moviesUpComing => _moviesUpComing.stream;

  fetch() async {
    _repository.getTopRate().then((it) {
      _moviesTopRate.sink.add(it);
    });
    _repository.getUpComing().then((it) {
      _moviesUpComing.sink.add(it);
    });
  }

  getMovie(int id) {
    return Stream.fromFuture(_repository.getById(id));
  }

  getMovieStream(MovieType type) {
    if (type == MovieType.topRate) {
      return moviesTopRate;
    }
    return moviesUpComing;
  }

  dispose() {
    _moviesTopRate.close();
    _moviesUpComing.close();
  }
}

final bloc = MovieBloc();
