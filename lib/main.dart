import 'package:flutter/material.dart';
import 'package:movies_app/ui/movie_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('TMDB'),
            bottom: TabBar(tabs: [
              Tab(text: "Top Rate"),
              Tab(text: "Up Coming"),
            ]),
          ),
          body: TabBarView(
            children: [
              MovieListScreen(MovieType.topRate),
              MovieListScreen(MovieType.upComing),
            ],
          ),
        ),
      ),
    );
  }
}
